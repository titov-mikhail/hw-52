import './App.css';
import {Component} from "react";
import CardComponent from './Cards/CardComponent';
import {getCardDeck, getRandomCards} from "./Cards/CardDeck";
import PokerHand from "./Combination/PokerHand";

class App extends Component {

    state = {
        cardDeck: getCardDeck(),
        onHandCards: undefined,
        canChangeCards: false
    }

    getDummyCards = () => {
        const  dummies = [];
        for(let i=0; i<5; i++){
            dummies.push(
                <div className="card little joker">
                    <span className="rank">-</span>
                    <span className="suit">Joker</span>
                </div>
            );
        }

        return dummies;
    }
    uncheck = (cards) => {
        for(let i = 0; i< cards.length; i++){
            const id = cards[i].getId();
            document.getElementById(id).checked = false;
        }
    }
    changeCards = () => {
        this.setState({canChangeCards: false});
        const cardDeck = [...this.state.cardDeck];
        let onHandCards = [...this.state.onHandCards];
        const removedIndexes = [];
        this.uncheck(onHandCards);
        let leftOnHandCards = this.removeCards([...onHandCards], removedIndexes);

        const randomCards = getRandomCards(cardDeck, removedIndexes.length);
        for (let i = 0; i < removedIndexes.length; i++) {
            leftOnHandCards[removedIndexes[i]] = randomCards.shift();
        }
        const hand = new PokerHand([...leftOnHandCards]);
        const gameResult = hand.getOutcome();

        this.setState({
            onHandCards: [...leftOnHandCards],
            cardDeck: [...cardDeck],
            gameResult: gameResult
        });
    };

    removeCards = (cards, removedIndexes) => {
        for (let i=0; i<cards.length; i++) {
           if(cards[i].checked){
               delete cards[i];
               removedIndexes.push(i);
           }
        }
        return cards;
    }

    startGame=()=>{
        const cardDeck = getCardDeck();
        const randomCards = getRandomCards(cardDeck, 5);
        this.setState({
            onHandCards: [...randomCards],
            cardDeck: [...cardDeck],
            canChangeCards: true,
            gameResult: undefined
        });
    }

  render() {
    return (
        <div className="game">
            <div className="playingCards">
                <ul className="table">
                {
                            this.state.onHandCards ?
                            this.state.onHandCards.map((c) =>
                            <li>
                                <CardComponent
                                    rank={c.rank }
                                    suit={c.suit }
                                    card = {c}
                                />
                            </li>
                            )
                        :
                            <li>
                                {this.getDummyCards()}
                            </li>
                    }
                </ul>
            </div>
            <div className="buttons">
                <div id='btnGenerate'>
                    <button onClick={this.startGame}>New game</button>
                </div>
                <div id='btnChange'>
                    <button onClick={this.changeCards} disabled={!this.state.canChangeCards}>Change cards / End game</button>
                </div>
            </div>

            <div>
                 {this.state.gameResult ? `Game result: ${this.state.gameResult}`: ""}
            </div>

        </div>
    );
  }
}

export default App;
