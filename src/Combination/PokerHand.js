import {ranks} from "../Cards/CardDeck";

class PokerHand {
    constructor(cards) {
        this.cards = cards;
        this.combs = [
            'royal',
            'straight-flush',
            'four',
            'full',
            'flush',
            'straight',
            'three',
            'two',
            'pair'
        ]
    }

    getOutcome=()=>{
        for (let i=0; i<this.combs.length; i++){
            switch (this.combs[i]){
                case 'royal':
                    if(this.checkRoyal()){
                        return 'Royal flush';
                    }
                    break;
                case 'straight-flush':
                    if(this.checkStraightFlush()){
                        return 'Straight flush';
                    }
                    break;
                case 'four':
                    if(this.checkFour()){
                        return 'Four of kind';
                    }
                    break;
                case 'full':
                    if (this.checkFull()) {
                        return 'Full house';
                    }
                    break;
                case 'flush':
                    if(this.checkFlush()){
                        return 'Flush';
                    }
                    break;
                case 'straight':
                    if( this.checkStraight()) {
                        return 'Straight';
                    }
                    break;
                case 'three':
                    if( this.checkThree()){
                        return 'Three of kind';
                    }
                    break;
                case 'two':
                    if(this.checkTwo()) {
                        return 'Two pairs';
                    }
                    break;
                case 'pair':
                    if(this.checkPair()) {
                        return 'One pair'
                    }
                    break;
                default:
                    return 'No combination';
            }
        }
    }

    checkRoyal =() => {
        if(!this.isFlush()){
            return false;
        }

        return this.getRankCount('A') === 1
            && this.getRankCount('K') === 1
            && this.getRankCount('Q') === 1
            && this.getRankCount('J') === 1
            && this.getRankCount('10') === 1;
    }

    checkStraightFlush =() => {
        return this.isStraight() && this.isFlush();
    }

    checkFour =() => {
        return this.getRankCount('A') === 4
            || this.getRankCount('K') === 4
            || this.getRankCount('Q') === 4
            || this.getRankCount('J') === 4
            || this.getRankCount('10') === 4
            || this.getRankCount('9') === 4
            || this.getRankCount('8') === 4
            || this.getRankCount('7') === 4
            || this.getRankCount('6') === 4
            || this.getRankCount('5') === 4
            || this.getRankCount('4') === 4
            || this.getRankCount('3') === 4
            || this.getRankCount('2') === 4;
    }

    checkFull =() => {
        const ranksCollection = this.getRanksCollection();
        let hasThree = false;
        let hasTwo = false;
        for (let i = 0; i < ranksCollection.length; i++){
            if(ranksCollection[i] === 3){
                hasThree = true;
            } else if (ranksCollection[i] === 2){
                hasTwo = true;
            }
        }

        return hasThree && hasTwo;
    }

    checkFlush =() => {
       return this.isFlush();
    }

    checkStraight =() => {
        return this.isStraight();
    }

    checkThree = ()=>{
        return this.getRankCount('A') === 3
            || this.getRankCount('K') === 3
            || this.getRankCount('Q') === 3
            || this.getRankCount('J') === 3
            || this.getRankCount('10') === 3
            || this.getRankCount('9') === 3
            || this.getRankCount('8') === 3
            || this.getRankCount('7') === 3
            || this.getRankCount('6') === 3
            || this.getRankCount('5') === 3
            || this.getRankCount('4') === 3
            || this.getRankCount('3') === 3
            || this.getRankCount('2') === 3;
    }

    checkTwo = () =>{
        const ranksCollection = this.getRanksCollection();
       let pairCount = 0;
        for (let i=0; i<ranksCollection.length; i++){
            if(ranksCollection[i]===2){
               pairCount++;
            }
        }
         return pairCount === 2;
    }

    checkPair = ()=> {
        return this.getRankCount('A') === 2
            || this.getRankCount('K') === 2
            || this.getRankCount('Q') === 2
            || this.getRankCount('J') === 2
            || this.getRankCount('10') === 2
            || this.getRankCount('9') === 2
            || this.getRankCount('8') === 2
            || this.getRankCount('7') === 2
            || this.getRankCount('6') === 2
            || this.getRankCount('5') === 2
            || this.getRankCount('4') === 2
            || this.getRankCount('3') === 2
            || this.getRankCount('2') === 2;
    }

    getRankCount = (rank) => {
        let count = 0;
        for (let i=0; i<this.cards.length; i++){
            if(this.cards[i].rank === rank){
                count++;
            }
        }
        return count;
    }

    getRanksCollection = () =>{
        return [
            this.getRankCount('A'),
            this.getRankCount('K'),
            this.getRankCount('Q'),
            this.getRankCount('J'),
            this.getRankCount('10'),
            this.getRankCount('9'),
            this.getRankCount('8'),
            this.getRankCount('7'),
            this.getRankCount('6'),
            this.getRankCount('5'),
            this.getRankCount('4'),
            this.getRankCount('3'),
            this.getRankCount('2'),
        ];
    }

    isFlush=()=>{
        let suit = this.cards[0].suit;
        for (let i=1; i<this.cards.length; i++){
            if(this.cards[i].suit !== suit)
                return false;
        }
        return true;
    }

    isStraight = () => {
        const rankIndexes = [];
        for (let i = 0; i < this.cards.length; i++) {
            rankIndexes.push(ranks.indexOf(this.cards[i].rank));
        }

        rankIndexes.sort((a, b) => {
            return a > b ? -1 : 1;
        });

        for (let i = 1; i < rankIndexes.length; i++) {
            if (rankIndexes[i] - rankIndexes[i - 1] !== 1) {
                return false;
            }
        }
    }
}

export  default PokerHand;