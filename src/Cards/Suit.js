export const getSuite = suite => {
    switch (suite) {
        case 'D':
        case 'd':
            return 'diams';
        case 'H':
        case 'h':
            return 'hearts';
        case 'C':
        case 'c':
            return 'clubs';
        case 'S':
        case 's':
            return 'spades';
        default:
            throw Error('Suite is unknown');
    }
}

export const getSuiteSign=(suite) => {
    let s;
    switch (suite) {
        case 'D':
        case 'd':
            s= '&diams;';
            break;
        case 'H':
        case 'h':
            s= '&hearts;';
            break;
        case 'C':
        case 'c':
            s= '&clubs;';
            break;
        case 'S':
        case 's':
            s= '&spades;';
            break;
        default:
            throw Error('Suite is unknown');
    }

    return {__html: s};
}