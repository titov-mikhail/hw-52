import React from "react";
import {getSuite, getSuiteSign} from "./Suit";
import './cards.css';
import Card from "./Card";

const checkedChanged = (card) => {
    card.checked = !card.checked;
};
export const CardComponent = props => {
    const rank = props.rank.toLowerCase();
    const suite = getSuite(props.suit);
    const suiteSign = getSuiteSign(props.suit);
    const className = `card rank-${rank} ${suite}`;

    return (
      <div className={className}>
            <span className='rank'>{props.rank}</span>
            <span className='suite' dangerouslySetInnerHTML={suiteSign}/>
          <div>
              <input
                  type = "checkbox"
                  name ={`c-${props.rank}${props.suit}`}
                  id = {`c-${props.rank}${props.suit}`}
                  value = "select"
                  onChange = { () => {
                      checkedChanged(props.card)
                  }}
              />
          </div>
      </div>
    );
}

export default CardComponent;
