class Card {
    constructor(rank, suit, checked = false) {
        this.rank = rank;
        this.suit = suit;
        this.checked = checked;
    }

    getId = () => {
        return `c-${this.rank}${this.suit}`;
    }
}

export default Card;