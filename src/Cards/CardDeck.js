import Card from "./Card";
export const ranks = ['2','3','4','5','6','7','8','9','10', 'J', 'Q', 'K', 'A'];
export const getCardDeck = () => {
    const suits = ['S', 'H', 'D', 'C'];
    const cardArray = [];

    for(let i=0; i<suits.length; i++){
        for(let j=0; j<ranks.length; j++){
             cardArray.push(new Card(ranks[j], suits[i]));
        }
    }
    return cardArray;
}

export const getRandomCards = (cardDeck, count = 5) =>{
    const randomCards = [];
    for (let i = 0; i < count; i++) {
        const number = Math.round(Math.random() * cardDeck.length);
        randomCards.push({...cardDeck[number]})
        cardDeck.splice(number, 1);
    }
    return randomCards;
}